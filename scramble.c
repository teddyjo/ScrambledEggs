#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/file.h>

long get_file_length(FILE* fp) {
  fseek(fp, 0L, SEEK_END);
  long size = ftell(fp);
  rewind(fp);
  return size;
}

typedef struct char_stack_elem {
  char c;
  struct char_stack_elem *below;
} char_stack_elem;

typedef struct char_stack {
  char_stack_elem *top;
} char_stack;

typedef struct string_stack_elem {
  char *c;
  struct string_stack_elem *below;
} string_stack_elem;

typedef struct string_stack {
  string_stack_elem *top;
} string_stack;

void ASSERT(int x) {
  if(x == 0) {
    printf("ASSERTION FAILURE\n");
    exit(1);
  }
}

int char_stack_empty(char_stack cs) {
  return cs.top == NULL;
}

char pop_char(char_stack *cs) {
  ASSERT(!char_stack_empty(*cs));
  char c = cs->top->c;
  char_stack_elem *to_free = cs->top;
  cs->top = cs->top->below;
  free(to_free);
  return c;
}

void push_char(char_stack *cs, char c) {
  char_stack_elem *new = malloc(sizeof(char_stack_elem));
  new->c = c;
  new->below = cs->top;
  cs->top = new;
}

int string_stack_empty(string_stack ss) {
  return ss.top == NULL;
}

char *pop_string(string_stack *ss) {
  ASSERT(!string_stack_empty(*ss));
  char *c = ss->top->c;
  string_stack_elem *to_free = ss->top;
  ss->top = ss->top->below;
  free(to_free);
  return c;
}

void push_string(string_stack *ss, char *c) {
  string_stack_elem *new = malloc(sizeof(string_stack_elem));
  new->c = c;
  new->below = ss->top;
  ss->top = new;
}

int string_contains_char(char *s, char c) {
  char current = *s;
  while(current != 0) {
    if(current == c) return 1;
  }
  return 0;
}

int isnt_directory(char *s) {
  struct stat path_stat;
  stat(s, &path_stat);
  return S_ISREG(path_stat.st_mode);
}

void analyze_directory(char *current_directory, string_stack *files_found) {

  string_stack files;
  string_stack dirs;

  /* BEGIN CODE CITATION
   * http://stackoverflow.com/questions/612097/how-can-i-get-the-list-of-files-in-a-directory-using-c-or-c
  */
  DIR *dir;
  struct dirent *ent;
  if ((dir = opendir (current_directory)) != NULL) {
    /* print all the files and directories within directory */
    while ((ent = readdir (dir)) != NULL) {
      printf ("%s\n", ent->d_name);
      if(isnt_directory(ent->d_name)) {
	       push_string(&files, ent->d_name);
      }
      else {
        if(strcmp(ent->d_name, "..") == 1 && strcmp(ent->d_name, ".") == 1) {
	      push_string(&dirs, ent->d_name);
        }
      }
    }
    closedir (dir);
  } else {
    /* could not open directory */
    /* original code signals error, but we don't really care here */
  }
  /* END CODE CITATION */

  // BEGIN CODE BLOCK: Iterate through stack of dirs, call analyze_directory for each one

  while(!string_stack_empty(files)) {
    push_string(files_found, pop_string(&files));
  }

  //TODO: ADD CASE THAT current_directory DOES NOT END WITH '/'
  while(!string_stack_empty(dirs)) {
    char *floop = strcat(current_directory, pop_string(&dirs));
    analyze_directory(floop, files_found);
  }

  // END CODE BLOCK
}

void print_usage() {
  printf("Usage: scramble <filename>\n");
  printf("  <-r>     : recursively scramble each file in a directory\n");
  printf("  <-i> <x> : write random numbers over a file <x> times (default is 7)");
  printf("  <-p>     : don't delete scrambled files");
}

int main(int argc, char **argv) {
  if(argc == 1) {
    print_usage();
    return 0;
  }

  string_stack file_stack;
  string_stack dir_stack;

  int recursive_mode = 0;
  int num_iterations = 7;
  int preserve_files = 0;

  for(int i = 1; i < argc; i++) {
    //check arguments

    if(strcmp(argv[i], "-r") == 0) {
      recursive_mode = 1;
    }
    else if(strcmp(argv[i], "-i") == 0) {
      //TODO: PUT MORE ASSERTIONS HERE
      ASSERT(i < argc - 1);
      num_iterations = atoi(argv[i+1]);
      i++;
    }
    else if(strcmp(argv[i], "-p") == 0) {
      preserve_files = 1;
    }
    else {
      if(isnt_directory(argv[i])) {
        push_string(&file_stack, argv[i]);
      }
      else {
        push_string(&dir_stack, argv[i]);
      }
    }
  }
  if(recursive_mode) {
    while(!string_stack_empty(dir_stack)) {
      analyze_directory(pop_string(&dir_stack), &file_stack);
    }
  }
  else {
    if(!string_stack_empty(dir_stack)) {
      while(!string_stack_empty(dir_stack)) {
        printf("NOTE: not scrambling directory %s because -r flag is not on\n", pop_string(&dir_stack));
      }
    }
  }
  while(!string_stack_empty(file_stack)) {
    char *filename = pop_string(&file_stack);
    FILE *fp1;
    fp1 = fopen(filename, "r");
    if(fp1) {
      for(int i = 0; i < num_iterations; i++) {
        srand(time(NULL));
        fclose(fp1);
        FILE *fp;
        fpos_t p1;
        fp = fopen(filename, "r+");
        //long len = get_file_length(fp);
        //struct flock lock = {F_WRLCK, SEEK_SET, 0, 0, 0};
        /*if(fcntl(fileno(fp), F_GETLK, &lock) < 0) {
          printf("Error: file lock couldn't be retrieved.\n");
          printf("%s\n", filename);
        }
        struct flock lock = {F_WRLCK, SEEK_SET, 0, 0};
        if(fcntl(fileno(fp), F_SETLKW, &lock) < 0) {
          printf("Error: file lock couldn't be set.\n");
        }*/
        if(flock(fileno(fp), LOCK_EX)) {
          printf("Couldn't lock file");
        }
        fgetpos(fp, &p1);
        char c;
        char rando;
        int going = 1;
        int charCount = 1;
        char_stack CS;
        CS.top = NULL;
        while(going == 1) {
          fgetpos(fp, &p1);
          going = (c = fgetc(fp)) != EOF;
          if(going == 1) {
            if(char_stack_empty(CS)) {
              int r = rand();
              char *cinter = (char *) &r;
              push_char(&CS, cinter[0]);
              push_char(&CS, cinter[1]);
              push_char(&CS, cinter[2]);
              push_char(&CS, cinter[3]);
            }
            rando = pop_char(&CS);
            fsetpos(fp, &p1);
            fputc(rando, fp);
            charCount++;
          }
        }
        fclose(fp);
      }
      if(!preserve_files) {
        unlink(filename);
      }
    }
    else {
      //File doesn't exist
      printf("Error: filename %s doesn't exist\n", filename);
      return 1;
    }
  }
}
